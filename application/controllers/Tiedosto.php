<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('tiedosto_model');
        $this->load->library('form_validation');
        $this->load->library('pagination');
    }
    
    public function index()
    {
        $tiedostoja_sivulla = 3;
        $data['tiedostot'] = $this->tiedosto_model->hae_kaikki($tiedostoja_sivulla,$this->uri->segment(3));
        $config['base_url'] = site_url('tiedosto/index');
        $config['total_rows'] = $this->tiedosto_model->laske_tiedostot();
        $config['per_page'] = $tiedostoja_sivulla;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $data['main_content'] = 'tiedostolista_view';
        $this->load->view('template',$data);
    }
    
    public function lisaa() 
    {
        $data['main_content'] = 'lisaa_view';
        $this->load->view('template',$data);
    }
    
    public function tallenna()
    {   
        $config['upload_path'] = $this->config->item('upload_path');
        $config['allowed_types'] = '*';
        $config['max_size']='0';
        
        $this->load->library('upload',$config);
        
        $tiedostot = $this->tiedosto_model->hae_kaikki();
        
        foreach ($tiedostot as $tiedosto) {
            if ($tiedosto->tiedostonimi === $_FILES['tiedosto']['name']) {
                redirect('tiedosto/lisaa');
            }
        }
        
        if (!$this->upload->do_upload('tiedosto')) {
            throw new Exception($this->upload->display_errors());
        }
        else {
            $info = $this->upload->data();
            
            $data = array(
                'nimi' => $this->input->post('nimi'),
                'kuvaus' => $this->input->post('kuvaus'),
                'tiedostonimi' => $info['file_name']
            );
            
            $this->tiedosto_model->lisaa($data);
            redirect('tiedosto/index');
        }
        
    }
    
    public function poista($id) {
        $this->tiedosto_model->poista(intval($id));
        redirect('tiedosto/index');
    }
}