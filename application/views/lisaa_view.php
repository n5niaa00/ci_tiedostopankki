<h3>Lisää tiedosto</h3>
<form action="<?php print site_url() . '/tiedosto/tallenna';?>" method="post" enctype='multipart/form-data'>
    <div class="form-group">
        <label>Nimi</label>
        <input type="text" name="nimi" class="form-control" required>
    </div>
    <div class="form-group">
        <label>Kuvaus</label>
        <input type="textfield" name="kuvaus" class="form-control" required>
    </div>
    <div class="form-group">
        <label>Tiedosto</label>
        <input type="file" name="tiedosto" id='tiedosto'>
    </div>
    <div>
        <button>Tallenna</button>
    </div>
</form>