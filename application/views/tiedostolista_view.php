
<h3 class="left">Tiedostolista</h3>
<form class="right" method="get" action="<?php print site_url() . '/tiedosto/lisaa';?>">
<button type="submit" class="btn btn-primary">Lisää</button>
</form>

<table class="table">
    <tr>
        <th>Nimi</th>
        <th>Tiedosto</th>
        <th>Kuvaus</th>
        <th>Tallennettu</th>
        <th></th>
    </tr>
<?php
foreach ($tiedostot as $tiedosto) {
    print "<tr>";
    print "<td>$tiedosto->nimi</td>";
    print "<td><a href=" . base_url($this->config->item('upload_path') . $tiedosto->tiedostonimi) . ">$tiedosto->tiedostonimi<a></td>";
    print "<td>$tiedosto->kuvaus</td>";
    print "<td>$tiedosto->tallennettu</td>";
    print "<td>" . anchor("tiedosto/poista/$tiedosto->id","Poista") . "</td>";
    print "</tr>";
}
?>
</table>
<?php
echo $this->pagination->create_links();
?>