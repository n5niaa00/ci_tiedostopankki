<?php
class Tiedosto_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki($limit=NULL, $offset=NULL) {
        $this->db->limit($limit,$offset);
        $query = $this->db->get('tiedosto');
        return $query->result();
    }
    
    public function laske_tiedostot() {
        return $this->db->count_all('tiedosto');
    }
    
    public function lisaa($data) {
        $this->db->insert('tiedosto',$data);
    }
    
    public function poista($id) {
        $query = $this->db->get('tiedosto');
        $this->db->where('id',$id);
        
        $result = $query->result();
        
        unlink($this->config->item('upload_path') . $result[0]->tiedostonimi);
        $this->db->delete('tiedosto');
    }
}