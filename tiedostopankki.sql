drop database if exists tiedostopankki;

create database tiedostopankki;

use tiedostopankki;

create table tiedosto (
    id int primary key auto_increment,
    nimi varchar(50) not null,
    tiedostonimi varchar(100) not null,
    kuvaus varchar(50) not null,
    tallennettu timestamp default current_timestamp
);
